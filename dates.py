def overlaps(self, date_start, date_end, price_date_start, price_date_end):
        return ((date_start is None or price_date_end is None or date_start <= price_date_end)
                and (price_date_start is None or date_end is None or price_date_start <= date_end)
                and (date_start is None or date_end is None or date_start <= date_end)
                and (price_date_start is None or price_date_end is None or price_date_start <= price_date_end))
